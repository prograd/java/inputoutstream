import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class Subject {
    private Integer maths;
    private Integer science;
    private Integer physics;
    private Integer computer;
    private Integer chemistry;


    public void accept() throws IOException {
        Scanner scanner=new Scanner(System.in);
        System.out.println("enter marks of Maths");
        this.maths=scanner.nextInt();
        System.out.println("enter marks of science");
        this.science=scanner.nextInt();
        System.out.println("enter marks of physics");
        this.physics=scanner.nextInt();
        System.out.println("enter marks of computer");
        this.computer=scanner.nextInt();
        System.out.println("enter marks of chemistry");
        this.chemistry=scanner.nextInt();
        FileOutputStream fileOutputStream=new FileOutputStream("K:\\Springboot Projects\\InputOutputStream\\src\\text.txt");
        fileOutputStream.write(maths);
        fileOutputStream.write(science);
        fileOutputStream.write(physics);
        fileOutputStream.write(computer);
        fileOutputStream.write(chemistry);

        fileOutputStream.close();
        System.out.println("Written");
    }
    public void read() throws IOException {
        FileInputStream fileInputStream=new FileInputStream("K:\\Springboot Projects\\InputOutputStream\\src\\text.txt");
        int i=0;
        while((i=fileInputStream.read())!=-1){
            System.out.print(i);
        }
    }
}
