import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        Subject subject=new Subject( );
        subject.accept();
        subject.read();
    }
}
